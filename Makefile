#! /usr/bin/make -f
# -*- makefile -*-

KERNEL:=../kernel
STABLE_ML:=../linux-stable-ml
TARGETVER:=4.4.y 4.19.y
MAKEFILE_DIR := $(dir $(lastword $(MAKEFILE_LIST)))
 
define check_patches
 	cd ${KERNEL}; \
 	git checkout v$1-rc; \
 	git remote update origin ; \
 	git remote update stable ; \
 	git remote update stable-rc ; \
 	git reset --hard stable-rc/linux-$1; \
 	cd - \
 
 	./classify_failed_patches.py --file patch-list --kernel ${KERNEL}
 
 	mv applied.txt $1.applied.txt
 	mv toapply.txt $1.toapply.txt 
endef
 
all: $(TARGETVER)

update-patch-list:
	cd ${STABLE_ML}; git pull; \
	git log --pretty='%s' | grep "^\[PATCH]" > $(PWD)/patch-list

4.4.y: ${REPO} update-patch-list
	$(call check_patches,$@)

4.19.y: ${REPO} update-patch-list
	$(call check_patches,$@)
