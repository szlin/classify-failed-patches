#!/usr/bin/python3

# License: Expat (MIT)
# Copyright: 2018 Daniel Sangorrin

import argparse
import sys
import os
import git # sudo apt-get install python-git

# Usage:
# $ ./classify_failed_patches.py --file subjects.txt --kernel /path/to/4.4.y
#  -> subjects.txt lines format: ^[PATCH] subject..
#  -> other lines will be printed but not parsed
# $ tail -f toapply.txt
# $ tail -f applied.txt
parser = argparse.ArgumentParser(description='Classify failed patches')
parser.add_argument('-f', '--file', type=argparse.FileType('r'), required=True, help='list of patch subjects starting with [PATCH]')
parser.add_argument('-k', '--kernelpath', type=str, required=True, help='kernel folder to check')
args = parser.parse_args()

repo = git.Git(args.kernelpath)

with open('applied.txt', 'a+') as applied, open('toapply.txt', 'a+') as toapply:
    for line in args.file:
        line = line.rstrip()
        if line.startswith('[PATCH]'):
            subject = line[8:]
            subject = subject.replace('*', '\*')
            subject = subject.replace('[', '\[')
            subject = subject.replace(']', '\]')
            print (subject)
            exists = repo.log('--pretty=oneline', '--grep=%s' % (subject))
            if not exists:
                msg = '[TOAPPLY] %s' % (subject)
                print (msg)
                toapply.write(msg + '\n')
            else:
                msg = '[APPLIED] %s' % (subject)
                print (msg)
                applied.write(msg + '\n')
                print ('\t' + exists)
                applied.write('\t' + exists + '\n')
        else:
            if 'exists' not in globals():
                continue
            print (line)
            if not exists:
                toapply.write(line + '\n')
            if exists:
                applied.write(line + '\n')
        # for tail -f toapply.txt to work
        toapply.flush()
        applied.flush()
